function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="col-4">
      <div class="card shadow p-3 mb-5 bg-white rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
            ${startDate} - ${endDate}
        </div>
      </div>
    </div>
    `;
  }

  function error(){
    return `
    <div class="card shadow p-3 mb-5 bg-white rounded col-12">
        <div class="alert alert-warning" role="alert">
            Information not found, please try again later.
        </div>
    </div>
  `;

  }


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            let count = 0
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    console.log(details)
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const column = document.querySelector(`#conf_list`);
                    const startDate = new Date(details.conference.starts)
                    const startMonth = startDate.getMonth() + 1
                    const startDay = startDate.getDate()
                    const startYear = startDate.getFullYear()
                    const endDate = new Date(details.conference.ends)
                    const endMonth = endDate.getMonth() + 1
                    const endDay = endDate.getDate()
                    const endYear = endDate.getFullYear()
                    const start = `${startMonth}-${startDay}-${startYear}`
                    const end = `${endMonth}-${endDay}-${endYear}`
                    const location = details.conference.location.name
                    const html = createCard(title, description, pictureUrl, start, end, location);
                    column.innerHTML += html;
                }
              }
        } else {
            const column = document.querySelector(`#conf_list`);
            const err = error()
            column.innerHTML = err
            throw new Error('Response not ok');
        }
    } catch (error) {
        console.error('error', error);
    }
});