// import { NavLink } from "react-router-dom";
import Dropdown from 'react-bootstrap/Dropdown';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <a className="navbar-brand font-weight-bold" href="/">Conference GO!</a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown p-2">
              <Dropdown>
                <Dropdown.Toggle variant="primary" id="dropdown-basic">
                  Conferences
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item href="/attendees/new">Attend Conference</Dropdown.Item>
                  <Dropdown.Item href="/conferences/new">Create Conference</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </li>
            <li className="nav-item dropdown p-2">
              <Dropdown>
                <Dropdown.Toggle variant="primary" id="dropdown-basic">
                  Locations
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item href="/locations/new">Create Location</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </li>
            <li className="nav-item dropdown p-2">
              <Dropdown>
                <Dropdown.Toggle variant="primary" id="dropdown-basic">
                  Presentations
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item href="/presentations/new">Create Presentation</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </li>
          </ul>
        </div>
      </div>
    </nav>

  );
}

export default Nav;